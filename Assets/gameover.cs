﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameover : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioHolder AudioHolder;

    void OnEnable(){
        AudioHolder.play(AudioHolder.AudioType.gameover);
    }
    public void restart(){
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
