﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradePanel : MonoBehaviour
{
    public UpgradeManager upgradeManager;
    public Text moneyAmount;
    public Text errorText;
    public Text vanAmount;
    public Text vanPurchaseText;
    public Text capacityAmount;
    public Text capacityPurchase;
    public Text loadRate;
    public Text loadRatePurchase;

    public float errorTime = 3f;


    public void UpdateUI()
    {
        upgradeManager.UpdateValues();
        vanPurchaseText.text = "Purchase ($" + upgradeManager.vanUpgradeCost + ")";
        capacityPurchase.text = "Purchase ($" + upgradeManager.capacityUpgradeCost + ")";
        loadRatePurchase.text = "Purchase ($" + upgradeManager.loadRateCost + ")";

        moneyAmount.text = "$" + upgradeManager.money;
        vanAmount.text = "" + upgradeManager.numberOfVehicles;
        capacityAmount.text = "" + upgradeManager.vanCapacity;
        loadRate.text = "" + upgradeManager.boxesPerSecond;
    }

    public void PurchaseVan()
    {
        if (upgradeManager.money >= upgradeManager.vanUpgradeCost)
        {
            upgradeManager.money -= upgradeManager.vanUpgradeCost;
            upgradeManager.vanLevel++;
            upgradeManager.numberOfVehicles++;
            UpdateUI();
        }
        else
        {
            InsufficientFunds();
        }
    }

    public void PurchaseLoadRate()
    {
        if (upgradeManager.money >= upgradeManager.loadRateCost)
        {
            upgradeManager.money -= upgradeManager.loadRateCost;
            upgradeManager.bpsLevel++;
            UpdateUI();
        }
        else
        {
            InsufficientFunds();
        }
    }

    public void PurchaseCapacity()
    {
        if (upgradeManager.money >= upgradeManager.capacityUpgradeCost)
        {
            upgradeManager.money -= upgradeManager.capacityUpgradeCost;
            upgradeManager.capLevel++;
            UpdateUI();
        }
        else
        {
            InsufficientFunds();
        }
    }

    public void InsufficientFunds()
    {
        StopAllCoroutines();
        StartCoroutine(ShowError("Insufficient funds. Learn to count, you dumbass."));
    }

    public IEnumerator ShowError(string text)
    {
        errorText.text = text;
        float progress = 0f;
        while (progress < errorTime)
        {
            progress += Time.deltaTime;
            errorText.color = new Color(errorText.color.r, errorText.color.g, errorText.color.b, 1 - (progress / errorTime));
            yield return null;
        }
        errorText.text = "";
    }

    public void Close(){
        upgradeManager.StopUpgrade();
    }
}
