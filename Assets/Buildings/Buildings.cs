﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buildings : MonoBehaviour
{
    public List<GameObject> buildings;

    public GameObject buildingPrefab;
    public float heighestPoint;
    public float lowestPoint;
    public int gridX;
    public int gridY;
    public int spacing;
    public float roadThickness = 10f;

    void Start()
    {
        buildings = new List<GameObject>((gridX/spacing)*(gridY/spacing));

        for (int i = -(gridX / 2 + (spacing - spacing / 2)); i <= (gridX / 2) + (spacing - spacing / 2); i += spacing)
        {
            for (int j = -(gridY / 2 + (spacing - spacing / 2)); j <= (gridY / 2) + (spacing - spacing / 2); j += spacing)
            {
                float randomHeight = Random.Range(lowestPoint, heighestPoint);
                GameObject currentBuilding = Instantiate(buildingPrefab, new Vector3(i, randomHeight / 2, j), Quaternion.identity);
                Vector3 scale = new Vector3(spacing - roadThickness / 2, randomHeight, spacing - roadThickness / 2);
                currentBuilding.transform.localScale = scale;
                currentBuilding.transform.parent = this.transform;

                buildings.Add(currentBuilding);
            }
        }

    }
}
