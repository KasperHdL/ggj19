﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSelector : MonoBehaviour
{
    public Transform cameraTransform;

    public InputHandler inputHandler;

    public Transform selectionBox;

    public float height;

    public LayerMask vehicleMask;
    public LayerMask groundMask;

    private bool selectionStarted = false;
    private Vector3 startPoint;

    private MeshRenderer boxRenderer;

    private List<PlayerVehicle> selectedVehicles;
    private AudioHolder audioholder;

    void Start(){
        cameraTransform = Camera.main.transform;
        audioholder = cameraTransform.gameObject.GetComponent<AudioHolder>();

        boxRenderer = selectionBox.GetComponent<MeshRenderer>();
        boxRenderer.enabled = false;

        selectedVehicles = new List<PlayerVehicle>(16);

        if (inputHandler == null) inputHandler = GetComponent<InputHandler>();

    }

    void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            Vector3 groundPoint;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, groundMask))
            {
                groundPoint = new Vector3(hit.point.x, 0, hit.point.z);
            }
            else
            {
                return;
            }
            if (!selectionStarted)
            {
                startPoint = groundPoint;
                selectionStarted = true;
                boxRenderer.enabled = true;
                foreach(PlayerVehicle v in selectedVehicles){
                    MeshRenderer[] meshes = v.gameObject.GetComponentsInChildren<MeshRenderer>();
                    foreach(MeshRenderer mesh in meshes){
                        mesh.material.SetFloat("_Outline", 0f);
                        mesh.material.SetColor("_XrayColor",new Color(1f,0.5807173f,0f));
                        //mesh.material.SetInt("_ZTest", 1);
                    }
                }
                selectedVehicles.Clear();
            }


            Vector3 delta = groundPoint - startPoint;

            Vector3 mid = delta * 0.5f + startPoint;

            float halfHeight = height * 0.5f;

            selectionBox.position = new Vector3(mid.x, halfHeight, mid.z);

            selectionBox.localScale = new Vector3(Mathf.Abs(delta.x), height, Mathf.Abs(delta.z));

        }
        else if (selectionStarted)
        {
           
            selectionStarted = false;
            boxRenderer.enabled = false;

            //get vehicles and set them as selected
            Collider[] colliders = Physics.OverlapBox(selectionBox.position, selectionBox.localScale * 0.5f, selectionBox.rotation, vehicleMask);

            for (int i = 0; i < colliders.Length; i++)
            {

                PlayerVehicle vehicle = colliders[i].GetComponent<PlayerVehicle>();
                if(vehicle != null){
                    MeshRenderer[] meshes = vehicle.gameObject.GetComponentsInChildren<MeshRenderer>();
                    foreach(MeshRenderer mesh in meshes){
                        mesh.material.SetFloat("_Outline", 1.5f);
                        mesh.material.SetColor("_XrayColor",new Color(0f,0.05506635f,1f));
                        //mesh.material.SetInt("_ZTest", 0);
                    }
                    
                    selectedVehicles.Add(vehicle);
                    vehicle.selected = true;
                }
            }

            if (selectedVehicles.Count > 0)
            {
                audioholder.play(AudioHolder.AudioType.selecting);
                inputHandler.selectedVehicles = selectedVehicles.ToArray();
            }
            else
            {
                foreach (PlayerVehicle vehicle in inputHandler.selectedVehicles)
                {
                    vehicle.selected = false;
                }
                inputHandler.selectedVehicles = new PlayerVehicle[0];
            }

        }
    }
}
