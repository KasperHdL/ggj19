using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

class GameLoop : MonoBehaviour
{
    public static GameLoop instance;

    public UpgradeManager upradeManaer;
    public Text roundNumber;
    public Text roundTime;
    public Text roundPoints;

    [Header("Counters (Read only)")]
    public int currentPoints = 0;
    public static int currentRound = 0;
    public float currentRoundTime = 0;

    public GameObject gameOverCanvas;

    private int currentRoundGoal;
    private int currentRoundTimeLimit;

    void Start()
    {
        instance = this;
        currentRound = 1;
        currentRoundTime = 0;

        Settings.gameIsPaused = false;

        currentRoundGoal = (int)Settings.data.roundGoal.Evaluate(currentRound);
        currentRoundTimeLimit = (int)Settings.data.roundTimeLimit.Evaluate(currentRound);
        roundNumber.text = "Round " + currentRound;
        roundPoints.text = "0/" + currentRoundGoal;
    }

    void GameOver(){
        if(!zenMode){
            gameOverCanvas.SetActive(true);
            Time.timeScale = 0;
            Settings.gameIsPaused = true;
        }

    }

    void NewRound(int currentRound)
    {
        upradeManaer.StartUpgrade(currentRound);
        Settings.gameIsPaused = true;


        currentRoundGoal = (int)Settings.data.roundGoal.Evaluate(currentRound);
        currentRoundTimeLimit = (int)Settings.data.roundTimeLimit.Evaluate(currentRound);
        roundNumber.text = "Round " + currentRound;
        roundPoints.text = "0/" + currentRoundGoal;
    }

    bool once = true;
    bool zenMode = false;
    void Update()
    {
        if(currentPoints > currentRoundGoal && !zenMode){
            //new Round
            currentRound++;
            NewRound(currentRound);
        
        }else if(zenMode && currentPoints%20==0){
            currentPoints++;
            upradeManaer.StartUpgrade(currentRound);
        }else{

            currentRoundTime += Time.deltaTime;
            float timeLeft = currentRoundTimeLimit - currentRoundTime;
            TimeSpan result = TimeSpan.FromMinutes((int)timeLeft);
            string fromTimeString = result.ToString("hh':'mm");
            roundTime.text = "" + fromTimeString;
            if(!zenMode){
                roundPoints.text = currentPoints + "/" + currentRoundGoal;
            }else{
                roundPoints.text = currentPoints + "!";
            }
            

            if(timeLeft < 0 && once){
                once = false;
                GameOver();
            }
        }

    }

    public void AvtivateZenMode(){
        currentRoundTimeLimit = 0;
        zenMode = true;
        roundNumber.text = "ZEN!";
    }

    public void AddPoint(){
        currentPoints++;
        upradeManaer.money += (int)Settings.data.moneyPerBox;
    }
}
