﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MoveSpawner : MonoBehaviour
{
    public static MoveSpawner instance;

    public Buildings buildings;

    public GameObject collectionPointPrefab;
    public GameObject dropPointPrefab;


    [Range(1,20)]
    public int boxesPerSpawn;
    [Range(1,20)]
    public int boxesPerSpawnRandomness;

    [Range(1,20)]
    public int stacksPerSpawn;
    [Range(1,20)]
    public int stacksPerSpawnRandomness;

    public float delayBetweenSpawn;


    public int maxMoves = 5;
    public float nextSpawnTime = 0;

    public List<int> types;
    public List<CollectionPoint> collectionPoints;
    public List<DropPoint> dropPoints;

    public float minRadiusFromOtherPoints = 8;

    public List<int> fromBuildingIndex;
    public List<int> toBuildingIndex;

    public Color defaultColor;

    public int totalCount =0; 

    void Start()
    {
        instance = this;
    }

    void FixedUpdate()
    {
        bool forceSpawn = types.Count == 0;
        if((collectionPoints.Count < maxMoves && nextSpawnTime < Time.time) || forceSpawn){
            int type = getFreeType();
            if(type == -1)return;

            int fromIndex = GetBuildingIndex();
            fromBuildingIndex.Add(fromIndex);
            int toIndex = GetBuildingIndex();
            toBuildingIndex.Add(toIndex);

            //spawn Collection Point
            GameObject b = buildings.buildings[fromIndex];
            Vector3 p = b.transform.position;
            p.y = 0;

            Vector3 top = p + Vector3.up * b.transform.localScale.y; 

            GameObject g = Instantiate(collectionPointPrefab, p, Quaternion.identity);
            CollectionPoint cp = g.GetComponent<CollectionPoint>();
            collectionPoints.Add(cp);
            cp.spawnPoint = top;
            cp.icon.transform.position = top + Vector3.up * 8;

            boxesPerSpawn = (int)Settings.data.boxesPerSpawn.Evaluate(GameLoop.currentRound);

            cp.numBoxes = Random.Range(0,boxesPerSpawnRandomness) + boxesPerSpawn;
            cp.numStacks = Random.Range(0,stacksPerSpawnRandomness) + stacksPerSpawn;

            types.Add(type);

            cp.type = type;
            cp.moveSpawner = this;

            b.GetComponent<MeshRenderer>().material.color = Settings.data.colors[type% Settings.data.colors.Count];


            //SPAWN DROP POINT
            b = buildings.buildings[toIndex];
            p = b.transform.position;
            p.y = 0;
            top = p + Vector3.up * b.transform.localScale.y; 

            g = Instantiate(dropPointPrefab, p, Quaternion.identity);
            DropPoint dp = g.GetComponent<DropPoint>();
            dropPoints.Add(dp);
            dp.maxBoxes = cp.numBoxes;
            dp.moveSpawner = this;
            dp.type = type;
            dp.icon.transform.position = top + Vector3.up * 8;

            b.GetComponent<MeshRenderer>().material.color = Settings.data.colors[type% Settings.data.colors.Count];

            delayBetweenSpawn = Settings.data.delayBetweenSpawn.Evaluate(GameLoop.currentRound);
            nextSpawnTime = Time.time + delayBetweenSpawn;
            totalCount++;
        }
    }


    int GetBuildingIndex(){
        int maxIt = 500;
        int it = 0;

        bool correct = false;
        int index = Random.Range(0, buildings.buildings.Count);
        if(fromBuildingIndex.Count == 0)return index;

        float sqRadius = Mathf.Pow(minRadiusFromOtherPoints, 2);
        while(!correct && it++ < maxIt){
            index = Random.Range(0, buildings.buildings.Count);
            Vector3 point = buildings.buildings[index].transform.position;
            correct = true;

            for(int i = 0 ; i < fromBuildingIndex.Count; i++){
                float d = Vector3.SqrMagnitude(buildings.buildings[fromBuildingIndex[i]].transform.position - point);
                if(d < sqRadius) {
                    index = -1;
                    correct = false;
                    Debug.DrawLine(point + Vector3.up * 10, buildings.buildings[fromBuildingIndex[i]].transform.position, Color.red, 5f);
                    break;
                }
                Debug.DrawLine(point + Vector3.up * 10, buildings.buildings[fromBuildingIndex[i]].transform.position, Color.green, .1f);

            }
            if(correct){
                for(int i = 0 ; i < toBuildingIndex.Count; i++){
                float d = Vector3.SqrMagnitude(buildings.buildings[toBuildingIndex[i]].transform.position - point);
                    if(d < sqRadius) {
                        index = -1;
                        correct = false;
                        Debug.DrawLine(point + Vector3.up * 10, buildings.buildings[toBuildingIndex[i]].transform.position, Color.red, 5f);
                        break;
                    }
                    Debug.DrawLine(point + Vector3.up * 10, buildings.buildings[toBuildingIndex[i]].transform.position, Color.green, .1f);
                }
            }
        }
        return index;
    }

    public void MoveComplete(int type){
        int index= -1;
        for(int i = 0 ; i < types.Count; i ++){
            if(types[i] == type) {
                index = i ;
                break;
            }
        }
        if(index == -1)Debug.LogError("...");


        Destroy(collectionPoints[index]);
        Destroy(dropPoints[index]);

        collectionPoints.RemoveAt(index);
        dropPoints.RemoveAt(index);


        buildings.buildings[fromBuildingIndex[index]].GetComponent<MeshRenderer>().material.color = defaultColor;
        buildings.buildings[toBuildingIndex[index]].GetComponent<MeshRenderer>().material.color = defaultColor;

        fromBuildingIndex.RemoveAt(index);
        toBuildingIndex.RemoveAt(index);

        types.RemoveAt(index);
    }

    public int getFreeType(){
        for(int i = 0; i < maxMoves;i++){
            bool found = false;
            for(int j = 0; j < types.Count;j++){
                if(types[j] == i){
                    found = true;
                    break;
                }
            }

            if(!found) return i;
        }

        return -1;

    }
}
