﻿using UnityEngine;

public class Hover : MonoBehaviour
{

    public bool useUnscaledTime = false;
    public float amount;
    public float speed = 1;

    private float startY;
    private float startTime;
    // Start is called before the first frame update
    void Start()
    {
        startTime = useUnscaledTime ? Time.unscaledTime : Time.time;
        NewHoverY();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 p = transform.localPosition;
        float t = useUnscaledTime ? Time.unscaledTime : Time.time;
        p.y = startY + amount + Mathf.Sin(speed *(t - startTime)) * amount;
        transform.localPosition = p;
    }
    public void NewHoverY(){
        startY = transform.localPosition.y;
    }
}
