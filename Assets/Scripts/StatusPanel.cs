﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusPanel : MonoBehaviour
{
    Text numberText;
    Renderer boxRend;
    void UpdatePanel(int number, Color color)
    {
        boxRend.material.color = color;
        numberText.text = "" + number;
    }
}
