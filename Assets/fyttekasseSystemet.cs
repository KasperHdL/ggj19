﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class fyttekasseSystemet : MonoBehaviour
{
    [Header("LEGENDARY!")]
    public GameObject legendary_object;
    public float legendary_spawnrate;
    [Range(0,100)]
    public int legendary_spawnpercentage;
    [Header("Epic!")]
    public GameObject epic_object;
    public float epic_spawnrate;
    [Range(0,100)]
    public int epic_spawnpercentage;
    [Header("Rare")]
    public GameObject rare_object;
    public float rare_spawnrate;
    [Range(0,100)]
    public int rare_spawnpercentage;
    [Header("Uncommon!")]
    public GameObject uncommon_object;
    public float ucommon_spawnrate;
    [Range(0,100)]
    public int ucommon_spawnpercentage;
    [Header("Common")]
    public GameObject common_object;
    public float common_spawnrate;
    [Range(0,100)]
    public int common_spawnpercentage;
    [Header("Other stuff")]
    protected float gridXWithSpacing;
    protected float gridYWithSpacing;
    public bool gameRunning;
    // Start is called before the first frame update
    void Start()
    {
        Buildings buildings = this.GetComponentInParent<Buildings>();
        gridXWithSpacing = (float)buildings.gridX;
        gridYWithSpacing = (float)buildings.gridY;
        StartCoroutine(spawnCrates(common_object, common_spawnrate,common_spawnpercentage));
    }

    public IEnumerator spawnCrates(GameObject rarity, float spawnRate, float spawnpercentage){
        while(gameRunning){
            yield return new WaitForSeconds(spawnRate);
            int rand = Random.Range(0,100);
            if(spawnpercentage>rand){
                GameObject temp = Instantiate(rarity,RandomNavmeshLocation(), Quaternion.identity);
            }
        }
        Debug.Log(RandomNavmeshLocation());
        yield return null;
    }

     public Vector3 RandomNavmeshLocation() {
         float x = Random.Range(-(gridXWithSpacing/2),(gridXWithSpacing/2));
         float y = Random.Range(-(gridYWithSpacing/2),(gridYWithSpacing/2));
         Vector3 randomPositionOnNavMesh = new Vector3(x,0,y);
         randomPositionOnNavMesh += transform.position;
         NavMeshHit hit;
         if (NavMesh.SamplePosition(randomPositionOnNavMesh, out hit, Mathf.Infinity, 1)) {
             return hit.position;            
         }
         return Vector3.zero;
     }
}
